#include "config.h"

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <stdbool.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>

#ifdef HAVE_SYS_SYSCALL_H
#include <sys/syscall.h>
#endif
#ifdef HAVE_SYSCALL_H
#include <syscall.h>
#endif

#include "uwrap_fake_socket_wrapper.h"

static void test_uwrap_syscall_swrap(void **state)
{
	long int rc;

	(void) state; /* unused */

	rc = syscall(__FAKE_SOCKET_WRAPPER_SYSCALL_NO);
	assert_int_equal(rc, __FAKE_SOCKET_WRAPPER_SYSCALL_RC);

	signal(SIGSYS, SIG_IGN);
	rc = syscall(__FAKE_SOCKET_WRAPPER_SYSCALL_NO+1);
	signal(SIGSYS, SIG_DFL);
	assert_int_equal(rc, -1);
	assert_int_equal(errno, ENOSYS);
}

int main(void) {
	int rc;

	const struct CMUnitTest uwrap_tests[] = {
		cmocka_unit_test(test_uwrap_syscall_swrap),
	};

	rc = cmocka_run_group_tests(uwrap_tests, NULL, NULL);

	return rc;
}
