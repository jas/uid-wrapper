uid-wrapper (1.3.1-2) unstable; urgency=medium

  [ Simon Josefsson ]
  * Add d/salsa-ci.yml.
  * Run wrap-and-sort -satbk.

  [ Helge Deller ]
  * Disable time64 transition.  Closes: #1086769.
  * Drop skip-arm-32bit-syscall.patch.

 -- Simon Josefsson <simon@josefsson.org>  Tue, 05 Nov 2024 19:14:25 +0100

uid-wrapper (1.3.1-1) unstable; urgency=medium

  * New upstream version.
  * Drop upstreamed fix-typo.diff, use-gnu-lib-names.diff,
    drop-unknown-syscall.diff.
  * DEP3ify skip-arm-32bit-syscall.patch.

 -- Simon Josefsson <simon@josefsson.org>  Mon, 23 Sep 2024 09:37:58 +0200

uid-wrapper (1.3.0-5.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian patch skip-arm-32bit-syscall.patch skips gettimeofday
    syscall test in test_syscall on 32-bit ARM (__arm__), since syscall
    __NR_gettimeofday returns tv2.tv_sec=0 on armel, armhf.
    Closes: #1069425.

 -- Drew Parsons <dparsons@debian.org>  Sun, 28 Jul 2024 12:25:35 +0200

uid-wrapper (1.3.0-5) unstable; urgency=medium

  [ Hefee ]
  * Enable verbose cmake in autopkgtest.

  [ Simon Josefsson ]
  * Bump Standards-Version: to 4.7.0.
  * Fix typo, silencing lintian.
  * Bump copyright years, silencing lintian.

 -- Simon Josefsson <simon@josefsson.org>  Tue, 23 Apr 2024 14:34:54 +0200

uid-wrapper (1.3.0-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on cmake (>= 2.8.8-3~).

 -- Simon Josefsson <simon@josefsson.org>  Sat, 21 Oct 2023 15:50:00 +0200

uid-wrapper (1.3.0-3) unstable; urgency=medium

  * control: Use new pkg-sssd email address.
  * drop-unknown-syscall.diff: Use upstream's patch.

 -- Simon Josefsson <simon@josefsson.org>  Tue, 08 Aug 2023 08:29:54 +0200

uid-wrapper (1.3.0-2) unstable; urgency=medium

  * drop-unknown-syscall.diff: Replaces syscall-unknown-errno.diff to fix
    FTBFS on armel/armhf.
  * use-gnu-lib-names.diff: Add.  Closes: #826906.

 -- Simon Josefsson <simon@josefsson.org>  Sat, 05 Aug 2023 21:00:40 +0200

uid-wrapper (1.3.0-1) unstable; urgency=medium

  [ Simon Josefsson ]
  * New upstream version.
  * control: Add myself as Uploaders.
  * upstream/metadata: Add.
  * copyright, watch: Drop obj32/ filtering, not needed any more.
  * watch: Add pgpsigurlmangle.
  * upstream/signing-key.asc: Add.
  * control: Standards-Version 4.6.2.
  * control: Rules-Requires-Root: no.
  * copyright: Add myself for debian/*, silencing
    lintian warning update-debian-copyright.
  * no-soname.diff: Mark as Forwarded: not-needed.
  * libuid-wrapper.lintian-overrides: Fix syntax.
  * control: Mark libuid-wrapper as Multi-Arch: same.
  * syscall-unknown-errno.diff: Add.
  * tests: Update LD_PRELOAD sed.  Output errors on failure.

  [ Debian Janitor <janitor@jelmer.uk> ]
  * Use secure copyright file specification URI.
  * Use secure URI in Homepage field.
  * Remove unnecessary XS-Testsuite field in debian/control.
  * Use canonical URL in Vcs-Browser.
  * Replace use of deprecated $ADTTMP with $AUTOPKGTEST_TMP.

 -- Simon Josefsson <simon@josefsson.org>  Fri, 04 Aug 2023 13:43:30 +0200

uid-wrapper (1.2.9-1) unstable; urgency=medium

  * New upstream release.
  * patches: Refreshed.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 26 Aug 2022 12:23:27 +0300

uid-wrapper (1.2.8-1) unstable; urgency=medium

  [ Timo Aaltonen ]
  * New upstream release.
  * Migrate to debhelper-compat, bump to 13.
  * no-soname.diff: Refreshed.
  * docs: Updated.

  [ Sergio Durigan Junior ]
  * d/rules: Modernize it with dh sequencer; use hardening when compiling.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 28 Jan 2021 14:31:52 +0200

uid-wrapper (1.2.4+dfsg1-1) unstable; urgency=medium

  * New upstream release. (Closes: #894794)
  * Bump debhelper compat to 11.
  * control: Bump policy to 4.1.3.
  * control: Update vcs urls.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 04 Apr 2018 14:08:05 +0300

uid-wrapper (1.2.1+dfsg1-1) unstable; urgency=medium

  * New upstream release.
  * patches: Refreshed.
  * control: Bump policy to 3.9.8, no changes.
  * control: Use https vcs urls.

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 20 Sep 2016 09:10:28 +0300

uid-wrapper (1.2.0+dfsg1-1) unstable; urgency=medium

  * New upstream release.
  * patches: Remove fix-test-gid.diff, refresh no-soname.diff.
  * copyright, watch: Filter obj32/* from the tarball, rename source to
    match.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 09 Dec 2015 16:27:44 +0200

uid-wrapper (1.1.1-1) unstable; urgency=medium

  * New upstream release.
  * control: Change maintainer, update Vcs. (Closes: #782374)
  * patches: no-soname.diff updated, remove-access.patch removed (upstream).
  * fix-test-gid.diff: Fix a failing test.
  * control: Fix a typo in description.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 04 Sep 2015 11:55:24 +0300

uid-wrapper (1.0.2-2) unstable; urgency=medium

  * Add patch remove-access.patch: remove use of SYS_access not
    available on ARM64. Closes: #770307
  * Bump standards version to 3.9.6 (no changes).

 -- Jelmer Vernooij <jelmer@debian.org>  Sat, 22 Nov 2014 11:48:26 +0000

uid-wrapper (1.0.2-1) unstable; urgency=medium

  * New upstream release.

 -- Jelmer Vernooij <jelmer@debian.org>  Fri, 08 Aug 2014 21:07:45 +0200

uid-wrapper (1.0.1-1) unstable; urgency=low

  * Initial release. Closes: #755557

 -- Jelmer Vernooij <jelmer@debian.org>  Tue, 22 Jul 2014 02:46:19 +0200
